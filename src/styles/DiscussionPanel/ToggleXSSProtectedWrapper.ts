import styled from "styled-components";

export const ToggleXSSProtectedWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: end;
`;