export enum SortingOptions {
    NameAsc = 'NameAsc',
    AddingDateAsc = 'AddingDateAsc',
    CurrentPriceAsc = 'CurrentPriceAsc',
    ExpectedPriceAsc = 'ExpectedPriceAsc',
    NameDesc = 'NameDesc',
    AddingDateDesc = 'AddingDateDesc',
    CurrentPriceDesc = 'CurrentPriceDesc',
    ExpectedPriceDesc = 'ExpectedPriceDesc',
}