import {DiscussionPost} from "../../dataModels/Post";

export interface DiscussionForumPageState {
    posts: undefined | DiscussionPost[],
}