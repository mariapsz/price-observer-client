import styled from "styled-components";

export const Link = styled.a`
    font-weight: 900;
    font-size: 15px;
`;