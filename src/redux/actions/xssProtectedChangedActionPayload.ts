export interface XssProtectedChangedActionPayload {
    xssProtected: boolean,
}