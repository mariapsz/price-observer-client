import styled from "styled-components";

export const NewPostMessageInput = styled.textarea`
    height: 80px;
    width: 100%;
    border-color: #e0e0e0;
    border-radius: 2px;
    border-width: 1px;
`;

export const NewPostLinkInput = styled.input`
    height: 30px;
    width: 100%;
    border-color: #e0e0e0;
    border-radius: 2px;
    border-width: 1px;
`;