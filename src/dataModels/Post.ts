export interface DiscussionPost {
    userName: string,
    message: string,
    link: string,
    date: number,
}