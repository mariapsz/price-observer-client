import styled from "styled-components";

export const PostContainerHeader = styled.div`
    display: flex;
    justify-content: space-between;
`;