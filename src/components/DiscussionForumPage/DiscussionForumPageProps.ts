export interface DiscussionForumPageProps {
    userName: string,
    token: string,
}